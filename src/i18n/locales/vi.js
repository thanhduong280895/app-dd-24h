export default {
  system: {
    header: {
      changeLanguage: 'Thay đổi ngôn ngữ',
    },
    tab: {
      dashboard: 'Thống kê',
      informationNutrition: 'Thông tin dinh dưỡng',
      meal: 'Khẩu phần ăn',
      setting: 'Cài đặt',
    },
  },
};
