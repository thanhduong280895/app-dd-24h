export default {
  system: {
    header: {
      changeLanguage: 'Change Language',
    },
    tab: {
      dashboard: 'Dashboard',
      informationNutrition: 'Information Nutrition',
      meal: 'Meal',
      setting: 'Setting',
    },
  },
};
