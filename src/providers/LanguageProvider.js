/* eslint-disable no-eval */
import React, {useState} from 'react';
import translation from '../i18n';
import {LanguageContext} from '../configs/context';

const LanguageProvider = (props) => {
  const [language, setLanguage] = useState('en');
  // eslint-disable-next-line no-unused-vars
  const fallbackTrans = translation.en;
  const [translations, setTranslations] = useState(translation[language]);

  const changeLanguage = (value) => {
    if (!translation[value]) {
      value = 'en';
    }
    setLanguage(value);
    setTranslations(translation[value]);
  };

  const trans = (text) => {
    try {
      const translatedText = eval(`translations.${text}`);
      if (translatedText) {
        return translatedText;
      }
      return text;
    } catch (e) {
      return eval(`fallbackTrans.${text}`);
    }
  };

  return (
    <LanguageContext.Provider
      value={{
        ...{language, translations},
        changeLanguage,
        trans,
      }}>
      {props.children}
    </LanguageContext.Provider>
  );
};

export default LanguageProvider;
