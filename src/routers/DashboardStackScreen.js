import React from 'react';
import Dashboard from '../screens/dashboard/Dashboard';
import {createStackNavigator} from '@react-navigation/stack';
import themes from '../configs/themes';
import {LanguageContext} from '../configs/context';

const DashBoardStack = createStackNavigator();

export const DashboardStackScreen = () => {
  const {trans} = React.useContext(LanguageContext);
  return (
    <DashBoardStack.Navigator>
      <DashBoardStack.Screen
        name="dashboard"
        component={Dashboard}
        options={{
          ...themes.headerOptions,
          title: trans('system.tab.dashboard'),
        }}
      />
    </DashBoardStack.Navigator>
  );
};
