import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import themes from '../configs/themes';
import InformationNutrition from '../screens/information-nutrition/InformationNutrition';

const InformationNutritionStack = createStackNavigator();

export const InformationNutritionStackScreen = () => {
  return (
    <InformationNutritionStack.Navigator>
      <InformationNutritionStack.Screen
        name="InformationNutrition"
        component={InformationNutrition}
        options={{
          ...themes.headerOptions,
        }}
      />
    </InformationNutritionStack.Navigator>
  );
};
