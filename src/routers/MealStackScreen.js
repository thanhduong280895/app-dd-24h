import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import themes from '../configs/themes';
import Meal from '../screens/meal/Meal';

const MealStack = createStackNavigator();

export const MealStackScreen = () => {
  return (
    <MealStack.Navigator>
      <MealStack.Screen
        name="Meal"
        component={Meal}
        options={{
          ...themes.headerOptions,
        }}
      />
    </MealStack.Navigator>
  );
};
