import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import themes from '../configs/themes';
import Setting from '../screens/setting/Setting';

const SettingStack = createStackNavigator();

export const SettingStackScreen = () => {
  return (
    <SettingStack.Navigator>
      <SettingStack.Screen
        name="Setting"
        component={Setting}
        options={{
          ...themes.headerOptions,
        }}
      />
    </SettingStack.Navigator>
  );
};
