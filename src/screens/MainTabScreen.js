import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import themes from '../configs/themes';
import {DashboardStackScreen} from '../routers/DashboardStackScreen';
import {SettingStackScreen} from '../routers/SettingStackScreen';
import {MealStackScreen} from '../routers/MealStackScreen';
import {InformationNutritionStackScreen} from '../routers/InformationNutritionStackScreen';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
import {StyleSheet, Platform} from 'react-native';
import {LanguageContext} from '../configs/context';
const Tab = createBottomTabNavigator();
const iconSize = 20;

const MainTabScreen = () => {
  const {trans} = React.useContext(LanguageContext);
  return (
    <Tab.Navigator
      initialRouteName="Dashboard"
      tabBarOptions={{
        activeTintColor: themes.mainColor,
        inactiveTintColor: themes.stepperBlur,
        style: styles.tabContainer,
      }}>
      <Tab.Screen
        name="Dashboard"
        component={DashboardStackScreen}
        options={{
          tabBarLabel: trans('system.tab.dashboard'),
          tabBarIcon: ({color}) => (
            <FontAwesome5 name="home" color={color} size={iconSize} />
          ),
        }}
      />
      <Tab.Screen
        name="InformationNutrition"
        component={InformationNutritionStackScreen}
        options={{
          tabBarLabel: trans('system.tab.informationNutrition'),
          tabBarIcon: ({color}) => (
            <FontAwesome5 name="cube" color={color} size={iconSize} />
          ),
        }}
      />
      <Tab.Screen
        name="Meal"
        component={MealStackScreen}
        options={{
          tabBarLabel: trans('system.tab.meal'),
          tabBarIcon: ({color}) => (
            <FontAwesome5 name="clipboard" color={color} size={iconSize} />
          ),
        }}
      />
      <Tab.Screen
        name="Setting"
        component={SettingStackScreen}
        options={{
          tabBarLabel: trans('system.tab.setting'),
          tabBarIcon: ({color}) => (
            <FontAwesome5 name="cog" color={color} size={iconSize} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default MainTabScreen;

const styles = StyleSheet.create({
  tabContainer: {
    backgroundColor: themes.whiteColor,
    height:
      55 +
      (Platform.OS === 'ios' ? StaticSafeAreaInsets.safeAreaInsetsBottom : 0),
  },
  fontWeight_500: {
    fontWeight: '500',
  },
});
