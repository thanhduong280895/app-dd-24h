import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
const InformationNutrition = () => {
  return (
    <View style={styles.container}>
      <Text>InformationNutrition</Text>
    </View>
  );
};

export default InformationNutrition;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
