import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
const Meal = () => {
  return (
    <View style={styles.container}>
      <Text>Meal</Text>
    </View>
  );
};

export default Meal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
