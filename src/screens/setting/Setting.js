import React, {useContext, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {LanguageContext} from '../../configs/context';
import themes from '../../configs/themes';
const Setting = () => {
  const {trans, changeLanguage} = useContext(LanguageContext);
  const [language, setLanguage] = useState('en');

  const changeLanguages = () => {
    setLanguage(language === 'en' ? 'vi' : 'en');
    changeLanguage(language);
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={changeLanguages}>
        <Text style={styles.buttonChange}>
          {trans('system.header.changeLanguage')}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Setting;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonChange: {
    backgroundColor: 'red',
    fontSize: 18,
    color: themes.whiteColor,
  },
});
