import React from 'react';
import MainTabScreen from '../src/screens/MainTabScreen';
import {NavigationContainer} from '@react-navigation/native';
import {navigationRef} from './routers/RootNavigation';
import LanguageProvider from './providers/LanguageProvider';
export const Route = () => {
  return (
    <LanguageProvider>
      <NavigationContainer ref={navigationRef}>
        <MainTabScreen />
      </NavigationContainer>
    </LanguageProvider>
  );
};
